
<< --License--

Licensed Materials - Property of IBM
(C) Copyright IBM Corporation 2017

--License--


#run scripts at relative path
scriptdir="$(dirname "$0")"
cd "$scriptdir"


echo ""
echo "Prerequisites"
echo "------------------------------------------------------------------------------------------------------"

cmd6=`docker ps 2>&1`
which docker > /dev/null 2>&1
if [ $? -eq 0 ]
echo ""
then
    docker --version > /dev/null 2>&1
    if [ $? -eq 0 ]
      then
        if [[ $cmd6 =~ "connect" ]] 
          then
           echo "Docker installed but not running. Please start docker or check that your user is a member of the docker group"
           echo "create docker group (groupadd docker)"
           echo "restart docker service (service docker restart)"
           echo "add user to docker group (usermod -a -G docker <user>)"
       else
       
          echo "Docker installed." 
          echo "Docker compose installed."
          echo "" 
echo ""

cmd=`docker ps  | grep transformation-advisor-server | awk '{print $1}'`
cmd1=`docker ps  | grep transformation-advisor-ui | awk '{print $1}'`
cmd2=`docker ps  | grep transformation-advisor-db | awk '{print $1}'`

cmd3=`docker images | grep transformation-advisor-server | awk '{print $1}'`
cmd4=`docker images | grep transformation-advisor-ui | awk '{print $1}'`
cmd5=`docker images | grep transformation-advisor-db | awk '{print $1}'`



if [[ -z $cmd3 ]] && [[ -z $cmd4 ]] && [[ -z $cmd5 ]];

then
 echo "Status"
 echo "------------------------------------------------------------------------------------------------------"
 echo "Transformation Advisor docker images are not pulled..."
 echo "Please select option 1 to install Transformation Advisor or option 7 if working in an Air Gapped Environment."
fi

if [[ ! -z $cmd3 ]];
	then
   	if [  -z $cmd ] || [  -z $cmd1 ] || [  -z $cmd2 ];
		then
 		echo "Status"
 		echo "------------------------------------------------------------------------------------------------------"
 		echo "Transformation Advisor docker images installed but not running."
                echo "Please select option 5 to start Transformation Advisor."
	else

          if uname -s | grep 'Darwin' > /dev/null ;


                   then

                   ## if word address is returned instead of IP

                   host_to_find=$(ifconfig | grep -Fv -- "-->" | grep "inet " | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | grep -Fv 127 | grep -Fv 255 | grep -Fv 0.0.0.0)

                     if [ -z "$host_to_find" ]; then


                      ui_port=$(docker ps | grep 3000  | awk -F "->" '{print $1}' | awk -F ":" '{print $3}')

##################################################### testing for couchDB #######

                        for (( i=0; i<150; i++ ))
                        do
                         db_port=$(docker ps | grep 5943  | awk -F "->" '{print $1}' | awk -F ":" '{print $3}')
                         if [ -z $db_port ]; then
                          echo "Status"
                          echo "---------------------------------------------------------------------------------------------------------------------------------"
                          echo     "Transformation Advisor couchDB pod could not be started."
                          echo     "This could indicate that the data directory does not have the correct permissions."
                          echo     "Run command 'chmod ugo+rwx data'."
                          echo     "Restart the couchDB pod and then the server pod."
                          break
                         fi
                        done

                      if [ -z $db_port ]; then

                          echo ""

                      fi


#######################################################################################
                     if [ ! -z $db_port ] ; then

                      uiID=`docker ps | grep transformation-advisor-ui | awk '{print $1}'`
                      taVersion=`docker inspect $uiID | grep version | tail -n 1 | awk -F : '{print $2}' |   sed 's/^.//' | tr -d '"'`

                      echo "Status"
                      echo "------------------------------------------------------------------------------------------------------"
                      echo     "Transformation Advisor $taVersion is available for us at the following URL> http://localhost:$ui_port"
                     fi

              else
                      host_to_find_array=( $host_to_find)

                      #sort array
                      host_to_find_array=( $(printf "%s\n" ${host_to_find_array[@]} | sort -r ) )

                      ui_port=$(docker ps | grep 3000  | awk -F "->" '{print $1}' | awk -F ":" '{print $3}')

 ##################################################### testing for couchDB #######

                        for (( i=0; i<150; i++ ))
                        do
                         db_port=$(docker ps | grep 5943  | awk -F "->" '{print $1}' | awk -F ":" '{print $3}')
                         if [ -z $db_port ]; then
                          echo "Status"
                          echo "---------------------------------------------------------------------------------------------------------------------------------"
                          echo     "Transformation Advisor couchDB pod could not be started."
                          echo     "This could indicate that the data directory does not have the correct permissions."
                          echo     "Run command 'chmod ugo+rwx data'."
                          echo     "Restart the couchDB pod and then the server pod."
                          break
                         fi
                        done

                        if [ -z $db_port ]; then

                          echo ""

                        fi


#######################################################################################



                        if [ ! -z $db_port ] ; then

                        uiID=`docker ps | grep transformation-advisor-ui | awk '{print $1}'`
                        taVersion=`docker inspect $uiID | grep version | tail -n 1 | awk -F : '{print $2}' |   sed 's/^.//' | tr -d '"'`

                         echo "Status"
                        echo "------------------------------------------------------------------------------------------------------"
                        echo     "Transformation Advisor $taVersion is available for us at the following URL> http://${host_to_find_array[0]}:$ui_port"
                       fi
                  
                      fi

####linux
		else
			ui_port=$(docker ps | grep 3000  | awk -F "->" '{print $1}' | awk -F ":" '{print $3}')
			
			######################################## testing for couchDB #######

                        for (( i=0; i<100; i++ ))
                        do
                         db_port=$(docker ps | grep 5943  | awk -F "->" '{print $1}' | awk -F ":" '{print $3}')
                         if [ -z $db_port ]; then
                          echo "Status"
                          echo "---------------------------------------------------------------------------------------------------------------------------------"
                          echo     "Transformation Advisor couchDB pod could not be started."
                          echo     "This could indicate that the data directory does not have the correct permissions."
                          echo     "Run command 'chmod ugo+rwx data'."
                          echo     "Restart the couchDB pod and then the server pod."
                          break
                         fi
                        done

###################################################################################################

                        host=`hostname -f`

                        if [ "$host" != "${host/.}" ] ;
                        then
                        host=`hostname -f`
                        else
                        host=`ip route get 8.8.8.8 | awk -F"src " 'NR==1{split($2,a," ");print a[1]}'`
                        fi

######################################## testing for couchDB #######

                      if [ -z $db_port ]; then

                          echo ""

                        fi

                    if [ ! -z $db_port ] ; then
  
                        uiID=`docker ps | grep transformation-advisor-ui | awk '{print $1}'`
                        taVersion=`docker inspect $uiID | grep version | tail -n 1 | awk -F : '{print $2}' |   sed 's/^.//' | tr -d '"'`


                        echo "Status"
                        echo "------------------------------------------------------------------------------------------------------"
                        echo     "Transformation Advisor $taVersion is available for us at the following URL> http://${host}:$ui_port"
		fi
		fi
		
	fi
fi

echo ""
echo ""
echo "" 
echo "Select the operation....... 

1) Install Transformation Advisor 
2) Uninstall Transformation Advisor (keep database data)
3) Uninstall Transformation Advisor (remove database data)
4) Stop Transformation Advisor
5) Start Transformation Advisor
6) Check for latest Transformation Advisor
7) Working in an Air Gapped Environment 
8) Quit "

read n
case $n in
    1) echo -n "Installing Transformation Advisor..."; ./cleanUpBeforeInstall.sh;   ./installTALocal.sh ; sleep 3; ./copy_back_files.sh; ./renameDockerName.sh &>/dev/null;;
    2) echo -n "Uninstalling Transformation Advisor..."; ./cleanUpBeforeInstall.sh  ; echo "Finished Uninstalling Transformation Advisor..." ; rm -rf ../.license_accepted ; rm -rf ../logs/* ; ./copy_back_files.sh;;
    3) echo -n "Uninstalling Transformation Advisor..."; ./cleanUpBeforeInstall.sh; rm -rf ../data; echo "Finished Uninstalling Transformation Advisor..." ; rm -rf ../.license_accepted ; rm -rf ../logs/* ; ./copy_back_files.sh;;
    4) echo -n "Stopping Transformation Advisor..."; ./stopTransformationAdvisor.sh; echo "Transformation Advisor processes are stopped.";; 
    5) echo "Starting Transformation Advisor..."; ./startTA.sh;; 
    6) echo -n "Checking if newer version of Transformation Advisor is available..."; ./checkforLatest.sh; echo "";;
    7) ./fileImages.sh;;
    8) exit ;; 
esac

 fi
     

else
 echo "Docker Enterprise Edition and Docker Community Edition versions 1.12 to 1.13.1 are supported. 
Docker Community Edition versions 17.03, 17.05, and 17.06 and Docker Enterprise Edition version 17.03 are also supported."
 echo ""
 echo "Install docker and docker compose before proceeding to install Transformation Advisor..." >&2

fi
fi 
#fi
