<< --License--

Licensed Materials - Property of IBM
(C) Copyright IBM Corporation 2017

--License--

#run scripts at relative path
scriptdir="$(dirname "$0")"
cd "$scriptdir"





echo "Have you already saved the Transformation Advisor images to a tarball (y/n)?"
read answer
echo ""
if [ $answer == 'y' ] || [ $answer == 'Y' ]; then

   while :
do
   echo "Enter filename location (eg, /root/ta/ta-images.tar.gz)"
   read -r location
   location="${location:=/root/ta/ta-images.tar.gz}"

   if  ! [[  "${location:0:1}" == / || "${location:0:2}" == ~[/a-z] ]]
      then
        location=$PWD/../$location
   fi

    if [[ -f $location ]] && [[ $location = *.gz ]]
    then
        break
    else
        echo "File ($location) does not exist. Try again."
    fi
done




echo -n "Loading the Transformation Advisor images into docker.."
while :;do echo -n .;sleep 1;done &
trap "kill $!" EXIT  2>/dev/null #Die with parent if we die prematurely
disown
gunzip -c "$location" | docker load
sleep 5
echo "" # or any other command here
kill $! && trap " " EXIT 2>/dev/null


 echo "Finished uploading the Transformation Advisor images"
 echo "Starting Transformation Advisor....."
 ./installTALocalAirGapped.sh
else

  echo "How to create the ta-images.tar.gz file."
  echo "--------------------------------------------"
  echo "On a system that has Docker installed and has internet connectivity, pull the TA docker images."
  echo "Then, save the docker images to a gzip'd tar ball."
  echo ""
  echo "docker pull ibmcom/transformation-advisor-db:latest"
  echo "docker pull ibmcom/transformation-advisor-ui:latest"
  echo "docker pull ibmcom/transformation-advisor-server:latest"
  echo ""
  echo "docker save ibmcom/transformation-advisor-db:latest ibmcom/transformation-advisor-ui:latest ibmcom/transformation-advisor-server:latest | gzip > ta-images.tar.gz"
  echo ""
  echo "Transfer the file to the target server and run the launchTransformationadvisor script."
  echo "Choose the option to work in an air gapped environment and you will be prompted to enter the location of the tarball containing the images."
  echo ""

  rm -rf ../.license_accepted

fi

