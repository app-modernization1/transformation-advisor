<< --License--

Licensed Materials - Property of IBM
(C) Copyright IBM Corporation 2017

--License--


#### Rename Docker Name ######## 

#############################

### Get the TA Docker Container IDs

trans=`docker ps -a | grep transformation-advisor-server | awk '{print $1}'`
couch=`docker ps -a | grep transformation-advisor-db | awk '{print $1}'`
ui=`docker ps -a | grep transformation-advisor-ui | awk '{print $1}'`

docker rename $trans ibm-transformationAdvisor-Server
docker rename $couch ibm-transformationAdvisor-couchDB
docker rename $ui ibm-transformationAdvisor-UI
