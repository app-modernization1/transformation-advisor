<< --License--

Licensed Materials - Property of IBM
(C) Copyright IBM Corporation 2017

--License--

#run scripts at relative path
scriptdir="$(dirname "$0")"
cd "$scriptdir"


#clean up before install

mkdir -p ../logs

while :;do echo -n .;sleep 1;done &
trap "kill $!" EXIT  2>/dev/null #Die with parent if we die prematurely
disown
./cleanBeforeInstallTA.sh > ../logs/uninstall_`date +"%Y%m%d_%H%M%S"`.log
echo "" # or any other command here
kill $! && trap " " EXIT 2>/dev/null
