<< --License--

Licensed Materials - Property of IBM
(C) Copyright IBM Corporation 2017

--License--

#run scripts at relative path
scriptdir="$(dirname "$0")"
cd "$scriptdir"

### Get the TA Docker Container IDs

ui=`docker ps -a | grep transformation-advisor-ui | awk '{print $1}'`
couch=`docker ps -a | grep transformation-advisor-db | awk '{print $1}'`
server=`docker ps -a | grep transformation-advisor-server | awk '{print $1}'`

#### Stop All TA Docker Containers


while :;do echo -n .;sleep 1;done &
trap "kill $!" EXIT  2>/dev/null #Die with parent if we die prematurely
disown

for a in $ui
do
docker stop $a > ../logs/uninstall_`date +"%Y%m%d_%H%M%S"`.log
done

for b in $couch
do
docker stop $b > ../logs/uninstall_`date +"%Y%m%d_%H%M%S"`.log
done

for c in $server
do
docker stop $c > ../logs/uninstall_`date +"%Y%m%d_%H%M%S"`.log
done

echo "" # or any other command here
kill $! && trap " " EXIT 2>/dev/null

