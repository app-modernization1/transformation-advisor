#run scripts at relative path
scriptdir="$(dirname "$0")"
cd "$scriptdir"

#set -e 
#trap 'echo >&2 "Issue occurred with pulling Transformation Advisor Docker Images. Please check you can access IBMCOM."; ' ERR


./findHost.sh
sleep 2 
./findPort.sh
sleep 2 

mkdir -p ../logs
sleep 1

#while :;do echo -n .;sleep 1;done &
#trap "kill $!" EXIT  2>/dev/null #Die with parent if we die prematurely
#disown
#docker-compose pull > ../logs/PullingcouchDB_`date +"%Y%m%d_%H%M%S"`.log
#docker-compose up -d > ../logs/InstallingTA_`date +"%Y%m%d_%H%M%S"`.log
docker-compose up -d

chmod ugo+rwx ../data
#echo "" # or any other command here
#kill $! && trap " " EXIT 2>/dev/null


echo -n "Configuring Transformation Advisor.."
while :;do echo -n .;sleep 1;done &
trap "kill $!" EXIT  2>/dev/null #Die with parent if we die prematurely
disown
sleep 45 
echo "" # or any other command here
kill $! && trap " " EXIT 2>/dev/null


./copy_back_files.sh


if uname -s | grep 'Darwin' > /dev/null ;


                   then


                   ## if word address is returned instead of IP

                   host_to_find=$(ifconfig | grep -Fv -- "-->" | grep "inet " | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | grep -Fv 127 | grep -Fv 255 | grep -Fv 0.0.0.0)

                    if [ -z "$host_to_find" ]; then


                      ui_port=$(docker ps | grep 3000  | awk -F "->" '{print $1}' | awk -F ":" '{print $3}')

################################################################ testing for couchDB #######

                        for (( i=0; i<150; i++ ))
                        do
                         db_port=$(docker ps | grep 5943  | awk -F "->" '{print $1}' | awk -F ":" '{print $3}')
                         if [ -z $db_port ]; then
                          echo "Status"
                          echo "---------------------------------------------------------------------------------------------------------------------------------"
                          echo     "Transformation Advisor couchDB pod could not be started."
                          echo     "This could indicate that the data directory does not have the correct permissions."
                          echo     "Run command 'chmod ugo+rwx data'."
                          echo     "Restart the couchDB pod and then the server pod."
                          exit
                         fi
                        done

################################################################################################################

                        uiID=`docker ps | grep transformation-advisor-ui | awk '{print $1}'`
                        taVersion=`docker inspect $uiID | grep version | tail -n 1 | awk -F : '{print $2}' |   sed 's/^.//' | tr -d '"'`

                      echo "Status"
                      echo "------------------------------------------------------------------------------------------------------"
                      echo     "Transformation Advisor $taVersion is available for us at the following URL> http://localhost:$ui_port"


                    else
                      host_to_find_array=( $host_to_find)

                      #sort array
                      host_to_find_array=( $(printf "%s\n" ${host_to_find_array[@]} | sort -r ) )

                      ui_port=$(docker ps | grep 3000  | awk -F "->" '{print $1}' | awk -F ":" '{print $3}')

################################################################ testing for couchDB #######

                        for (( i=0; i<150; i++ ))
                        do
                         db_port=$(docker ps | grep 5943  | awk -F "->" '{print $1}' | awk -F ":" '{print $3}')
                         if [ -z $db_port ]; then
                          echo "Status"
                          echo "---------------------------------------------------------------------------------------------------------------------------------"
                          echo     "Transformation Advisor couchDB pod could not be started."
                          echo     "This could indicate that the data directory does not have the correct permissions."
                          echo     "Run command 'chmod ugo+rwx data'."
                          echo     "Restart the couchDB pod and then the server pod."
                          exit
                         fi
                        done

################################################################################################################

                  uiID=`docker ps | grep transformation-advisor-ui | awk '{print $1}'`
                  taVersion=`docker inspect $uiID | grep version | tail -n 1 | awk -F : '{print $2}' |   sed 's/^.//' | tr -d '"'`


                      echo "Status"
                      echo "------------------------------------------------------------------------------------------------------"
                      echo     "Transformation Advisor $taVersion is available for us at the following URL> http://${host_to_find_array[0]}:$ui_port"

                    fi

                else
                        ui_port=$(docker ps | grep 3000  | awk -F "->" '{print $1}' | awk -F ":" '{print $3}')
                        
                        ################### testing for couchDB #####################################

                        for (( i=0; i<150; i++ ))
                        do
                         db_port=$(docker ps | grep 5943  | awk -F "->" '{print $1}' | awk -F ":" '{print $3}')
                         if [ -z $db_port ]; then
                          echo "Status"
                          echo "---------------------------------------------------------------------------------------------------------------------------------"
                          echo     "Transformation Advisor couchDB pod could not be started."
                          echo     "This could indicate that the data directory does not have the correct permissions."
                          echo     "Run command 'chmod ugo+rwx data.'"
                          echo     "Rstart the couchDB pod and then the server pod."
                          exit
                         fi
                        done

 ###########################################################################


                        host=`hostname -f`

                        if [ "$host" != "${host/.}" ] ;
                        then
                        host=`hostname -f`
                        else
                        host=`ip route get 8.8.8.8 | awk -F"src " 'NR==1{split($2,a," ");print a[1]}'`
                        fi

                         uiID=`docker ps | grep transformation-advisor-ui | awk '{print $1}'`
                         taVersion=`docker inspect $uiID | grep version | tail -n 1 | awk -F : '{print $2}' |   sed 's/^.//' | tr -d '"'`

                        echo "Status"
                        echo "------------------------------------------------------------------------------------------------------"
                        echo     "Transformation Advisor $taVersion is available for us at the following URL> http://${host}:$ui_port"
                fi



