#run scripts at relative path
scriptdir="$(dirname "$0")"
cd "$scriptdir"

while :;do echo -n .;sleep 1;done &
trap "kill $!" EXIT  2>/dev/null #Die with parent if we die prematurely
disown
docker pull ibmcom/transformation-advisor-ui:latest > ./latest.txt 
echo "" # or any other command here
kill $! && trap " " EXIT 2>/dev/null




if  `grep -q  "Image is up to date" ./latest.txt`; then

echo "Status"
       echo "------------------------------------------------------------------------------------------------------"
       echo     "Transformation Advisor is up to date."

else

echo "Status"
       echo "------------------------------------------------------------------------------------------------------"
       echo     "Newer version of Transformation Advisor is available. Please select option 2 or 3 to uninstall and "
       echo     "option 1 to install the latest version of Transformation Advisor."

       docker rmi -f ibmcom/transformation-advisor-ui:latest > ./uninstall.txt
  
       image=`docker images | grep '<none>' | awk '{print $3}'`

       process=`docker ps | grep 3000 | awk '{print $2}'`


       if [ $image == $process ]; then

         docker tag $image ibmcom/transformation-advisor-ui:latest &> /dev/null 

       fi


       rm -rf ./uninstall.txt 


fi


rm -rf ./latest.txt

