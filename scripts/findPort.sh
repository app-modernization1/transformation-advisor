<< --License--

Licensed Materials - Property of IBM
(C) Copyright IBM Corporation 2017

--License--

#run scripts at relative path
scriptdir="$(dirname "$0")"
cd "$scriptdir"


port_array=()
for i in {2220..2320}; do
   if netstat -an | grep $i | grep -i listen  |  read REPLY; then
     echo -n 
   else
     echo -n 
     port_array+=($i)
#     echo ${port_array[0]} is from array
#     echo ${port_array[1]} is from array
#     echo ${port_array[2]} is from array
   fi 
done

if uname -s | grep 'Darwin' > /dev/null;
	then 
	# sed commands modified for mac
	sed -i '' -e 's/<server_port>/'${port_array[0]}'/g' ./.env

        sed -i '' -e 's/<server_port>/'${port_array[0]}'/g' ./.env

	sed -i '' -e 's/<ui_port>/'${port_array[1]}'/g' ./.env

        sed -i '' -e 's/<ui_port>/'${port_array[1]}'/g' ./.env

	sed -i '' -e 's/<db_port>/'${port_array[2]}'/g' ./.env
else
	
	sed -i  's/<server_port>/'${port_array[0]}'/g' ./.env

        sed -i  's/<server_port>/'${port_array[0]}'/g' ./.env

	sed -i 's/<ui_port>/'${port_array[1]}'/g' ./.env

        sed -i 's/<ui_port>/'${port_array[1]}'/g' ./.env

	sed -i 's/<db_port>/'${port_array[2]}'/g' ./.env
fi
