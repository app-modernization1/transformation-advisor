<< --License--

Licensed Materials - Property of IBM
(C) Copyright IBM Corporation 2017

--License--

#### Clean up docker ######## 

#############################

### Get the TA Docker Container IDs

ui=`docker ps -a | grep transformation-advisor-ui | awk '{print $1}'`
couch=`docker ps -a | grep transformation-advisor-db | awk '{print $1}'`
server=`docker ps -a | grep transformation-advisor-server | awk '{print $1}'`



#### Stop All TA Docker Containers

for a in $ui
do
docker stop $a
done

for b in $couch
do
docker stop $b
done

for c in $server
do
docker stop $c
done



#### Remove TA Containers ###################


for d in $ui
do
  docker rm -f $d
done

for e in $couch
do
  docker rm -f $e
done

for f in $server
do
  docker rm -f $f
done



#### Remove TA Images #####################

ui_image=`docker images | grep transformation-advisor-ui | awk '{print $3}'`
couch_image=`docker images | grep transformation-advisor-db | awk '{print $3}'`
server_image=`docker images | grep transformation-advisor-server | awk '{print $3}'`


##ui_image=`docker images | grep ibmcom/icp-transformation-advisor-ui | awk '{print $3}'`
##couch_image=`docker images | grep transformation-advisor-db | awk '{print $3}'`
##server_image=`docker images | grep ibmcom/icp-transformation-advisor-server | awk '{print $3}'`


for g in $ui_image
do
docker rmi -f $g
done

for h in $couch_image
do
docker rmi -f $h
done

for i in $server_image
do
docker rmi -f $i
done


sleep 5

### Find the hostname and edit the environment files #######
############################################################

./findHost.sh

./findPort.sh

sleep 5
