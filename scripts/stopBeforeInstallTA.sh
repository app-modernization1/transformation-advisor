<< --License--

Licensed Materials - Property of IBM
(C) Copyright IBM Corporation 2017

--License--

#### Clean up docker ######## 

#############################

### Get the TA Docker Container IDs

ui=`docker ps -a | grep transformation-advisor-ui | awk '{print $1}'`
couch=`docker ps -a | grep transformation-advisor-db | awk '{print $1}'`
server=`docker ps -a | grep transformation-advisor-server | awk '{print $1}'`

#### Stop All TA Docker Containers

for a in $ui
do
docker stop $a
done

for b in $couch
do
docker stop $b
done

for c in $server
do
docker stop $c
done



sleep 5

### Find the hostname and edit the environment files #######
############################################################

./findHost.sh

./findPort.sh

sleep 5
