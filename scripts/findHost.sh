<< --License--

Licensed Materials - Property of IBM
(C) Copyright IBM Corporation 2017

--License--

#run scripts at relative path
scriptdir="$(dirname "$0")"
cd "$scriptdir"

#host=`hostname -f` 


#if [ "$host" != "${host/.}" ] ;
#then
#host=`hostname -f`
#else
#host=`ip route get 8.8.8.8 | awk -F"src " 'NR==1{split($2,a," ");print a[1]}'`
#fi



if uname -s | grep 'Darwin' > /dev/null ;
        then
      
        ## if word address is returned instead of IP

        host_to_find=$(ifconfig | grep -Fv -- "-->" | grep "inet " | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | grep -Fv 127 | grep -Fv 255 | grep -Fv 0.0.0.0)



        if [ -z "$host_to_find" ]; then

           sed -i '' -e 's/<host>/localhost/g' ./.env 

        else


          host_to_find_array=( $host_to_find)

        #sort array
          host_to_find_array=( $(printf "%s\n" ${host_to_find_array[@]} | sort -r ) )

          sed -i '' -e 's/<host>/'${host_to_find_array[0]}'/g' ./.env

       #### remove    sed -i 's/<host>/'${host_to_find_array[0]}'/g' ./.env
        fi

else
       
        host=`hostname -f`

        if [ "$host" != "${host/.}" ] ;
        then
        host=`hostname -f`
        else
        host=`ip route get 8.8.8.8 | awk -F"src " 'NR==1{split($2,a," ");print a[1]}'`
        fi

	
	sed -i 's/<host>/'${host}'/g' ./.env
	
fi
