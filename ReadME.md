
# 초기화 
최초에 한번 yum을 업데이트 한다.
```
yum -y update
```


host명 변경

hostname 으로 확인
hostnamectl set-hostname 169.56.76.169.nip.io
vi /etc/host 에서 변경 


# centos8에 docker install

###### docker install 
dnf를 사용하도록 설정한다.    
```
dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
```

dnf를 이용하여 docker를 설치한다.   
```
dnf install docker-ce docker-ce-cli containerd.io
```

설치된 docker가 서버가 재부팅 되어도 실행되도록 시스템에 등록한다.   
```
systemctl enable --now docker
```

###### 방화벽 
방화벽 설정을 한다.
```
firewall-cmd --zone=public --add-masquerade --permanent
```
설정된 정보를 적용한다. 
```
firewall-cmd --reload
```

###### docker-compose install
curl을 이용하여 docker-compose를 설치한다.   
```
curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

docker-compose를 다른 계정에서 사용하도록 권한을 부여한다.   
```
chmod +x /usr/local/bin/docker-compose
```

설치된 docker가 잘 동작하는지 확인한다.  
```
docker run hello-world
```

# user add
작업용 계정을 생성한다.  
```
useradd ta
passwd XXXXXX
```

docker를 그룹에 추가한다.  
```
groupadd docker
```
생성된 docker 그룹에 ta 계정을 추가한다.    
```
usermod -aG docker ta
```

# git install
dnf를 이용하여 git을 설치한다.   
```
dnf install git -y
dnf install unzip -y
```

# gitlab
Transformation Advisor를 설치하기 위해 설치 스크립트를 git에서 복사한다.   
```
git clone https://gitlab.com/app-modernization1/transformation-advisor.git
```


# transformation advisor install

참고 사이트
https://www.ibm.com/docs/en/cta?topic=started-deployment-options

설치용 파일을 다운 받는다.    
IBM로그인 된 계정으로 로컬에 설치할 수 있는 설치 스크립트를 다운 받는다.    

2021.6.25일 최신 버전은   
./utils/transformationAdvisor-linux.zip   
이다. 


위 파일을 압축 해제 하면 아래의 폴더가 생성된다.    
여기서는 /utils/transformation-advisor-local-2.4.4에서 진행한다.  


##### 2021.6.25. 최신 버전 인스톨   
설치 스크립트를 실행한다. 
```
../script/launchTransformationAdvisor.sh
```

1번을 선택하여 설치를 진행한다.


설치가 완료되면 엔드포트 정보를 확인한 후 웹 접속을 한다.   
```
http://169.56.76.169:3000/
```

http://169.56.76.169.nip.io:3000



# sample app
TA 샘플용 소스   
https://github.com/IBM/appmod-resorts

https://ibm-developer.gitbook.io/cloudpakforapplications-appmod/tools-for-modernization/app-modernization-ta-explore-lab-openshift4



# script
작동안함  
/Users/juheon/workspace-2021/transformation-advisor/transformation-advisor/utils
/Users/juheon/dev/ta/transformationadvisor-Linux_gg_server2/transformationadvisor-2.4.4

./bin/transformationadvisor --tomcat-apps-location /Users/juheon/workspace-2020/egovframework/egovframework/target/ROOT.war

 vi ~/.bash_profile
export JAVA_HOME=/Library/Java/JavaVirtualMachines/openjdk-11.0.2.jdk/Contents/Home/bin
export PATH=${PATH}:$JAVA_HOME
